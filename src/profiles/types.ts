import { IconName } from "./icons";

export type Profile = {
  pictureURI: string;
  name: string;
  title: string;
  contactNo: string;
  email: string;
  mapLink: string;
  about: {
    icon: IconName;
    label: string;
    description: string;
    link?: string;
  }[];
  links: {
    icon: IconName;
    label: string;
    description: string;
    link?: string;
  }[];
};

export type Profiles = {
  slug: string;
  profile: Profile;
}[];
