import { Profiles } from "./types";

const profiles: Profiles = [
  {
    slug: "someone",
    profile: {
      pictureURI: "/assets/profile.jpeg",
      name: "Someone",
      title: "Chief Executive Officer",
      contactNo: "+6666666666",
      email: "someone@someemail.com",
      mapLink:
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.8029686285404!2d101.61388846532725!3d3.1466335040442193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc492cc6659a39%3A0xc3a05b8140f287a2!2sFirst%20Ave%2C%20Bandar%20Utama%2C%2047800%20Petaling%20Jaya%2C%20Selangor!5e0!3m2!1sen!2smy!4v1670948125862!5m2!1sen!2smy",
      about: [
        {
          icon: "designation",
          label: "Designation",
          description: "CEO",
        },
        {
          icon: "company",
          label: "Company",
          description: "GamBit Group",
        },
        {
          icon: "website",
          label: "Website",
          description: "www.gambit.com.my",
          link: "https://www.gambit.com.my",
        },
        {
          icon: "address",
          label: "Address",
          description: "Level 18.02, 1, First Avenue, Bandar Utama, 47800 Petaling Jaya, Selangor, Malaysia",
        },
      ],
      links: [
        {
          icon: "instagram",
          label: "Instagram",
          description: "@someone",
          link: "https://instagram.com/someone",
        },
        {
          icon: "twitter",
          label: "Twitter",
          description: "@someone",
          link: "https://twitter.com/someone",
        },
        {
          icon: "linkedin",
          label: "LinkedIn",
          description: "someone",
          link: "https://www.linkedin.com/in/someone/",
        },
        {
          icon: "facebook",
          label: "Facebook",
          description: "someone",
          link: "https://facebook.com/someone/",
        },
      ],
    },
  },
];

export default profiles;
