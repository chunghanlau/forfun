import {
  FaBriefcase,
  FaEnvelope,
  FaFacebookF,
  FaGlobeAsia,
  FaInstagram,
  FaLinkedinIn,
  FaList,
  FaMapMarkerAlt,
  FaPhone,
  FaShareAlt,
  FaTwitter,
} from "react-icons/fa";

const icons = {
  designation: FaBriefcase,
  company: FaList,
  website: FaGlobeAsia,
  address: FaMapMarkerAlt,
  instagram: FaInstagram,
  twitter: FaTwitter,
  linkedin: FaLinkedinIn,
  facebook: FaFacebookF,
  phone: FaPhone,
  email: FaEnvelope,
  share: FaShareAlt,
};

export type IconName = keyof typeof icons;

export default icons;
