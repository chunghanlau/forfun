import { AspectRatio } from "@chakra-ui/react";

export default function MapBox({ mapLink }: { mapLink: string }) {
  return (
    <AspectRatio ratio={16 / 9} rounded="lg">
      <iframe
        src={mapLink}
        width="600"
        height="450"
        style={{ border: "1px solid rgba(0,0,0,0.25)", padding: "5px" }}
        allowFullScreen={false}
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
    </AspectRatio>
  );
}
