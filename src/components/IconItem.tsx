import { Box, Center, Flex, Icon, LinkBox, LinkOverlay, Text } from "@chakra-ui/react";
import { FaExternalLinkAlt } from "react-icons/fa";
import icons, { IconName } from "../profiles/icons";

export default function IconItem({
  label,
  description,
  icon,
  link,
}: {
  label: string;
  description: string;
  icon: IconName;
  link?: string;
}) {
  return (
    <LinkBox>
      <Flex alignItems="start" role="group">
        <Center
          minW={{ base: 6, md: 12 }}
          minH={{ base: 6, md: 12 }}
          border="1px solid"
          transition="all .1s ease-in-out"
          _groupHover={
            link
              ? {
                  bg: "gray.100",
                }
              : {}
          }
        >
          <Icon as={icons[icon]} fontSize={{ base: "sm", md: "xl" }} />
        </Center>
        <Box ml={{ base: 2, md: 5 }} textAlign="left">
          <Text fontFamily="mono" textTransform="uppercase" fontSize="xs" letterSpacing={"0.1em"}>
            {label}
            {link && <Icon as={FaExternalLinkAlt} ml={1} fontSize="xx-small" />}
          </Text>
          {link ? (
            <LinkOverlay href={link} target="_blank">
              <Text fontSize="lg" fontWeight="semibold" wordBreak="break-word">
                {description}
              </Text>
            </LinkOverlay>
          ) : (
            <Text fontSize="lg" fontWeight="semibold" wordBreak="break-word">
              {description}
            </Text>
          )}
        </Box>
      </Flex>
    </LinkBox>
  );
}
