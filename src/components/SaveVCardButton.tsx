import { Button } from "@chakra-ui/react";
import React from "react";
import { FaAddressBook } from "react-icons/fa";
import { Profile } from "../profiles/types";

export default function SaveVCardButton({ profile }: { profile: Profile }) {
  const saveCard = async (profile: Profile) => {
    let base64Image = "";
    if (profile.pictureURI) {
      const response = await fetch(profile.pictureURI);
      const blob = await response.blob();
      base64Image = await new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = () => {
          const str = reader.result as string;
          resolve(`PHOTO;TYPE=JPEG;ENCODING=BASE64:${str.split("base64,")[1]}\n`);
        };
        reader.onerror = reject;
        reader.readAsDataURL(blob);
      });
    }

    const address = profile.about.find((i) => i.label === "Address")?.description;
    const websiteLink = profile.about.find((i) => i.label === "Website")?.link;

    const vcard =
      "BEGIN:VCARD\n" +
      "VERSION:3.0\n" +
      `N;CHARSET=UTF-8: ; ${profile.name};;;\n` +
      `FN;CHARSET=UTF-8: ${profile.name}\n` +
      `TITLE;CHARSET=UTF-8:${profile.title}\n` +
      `ORG;CHARSET=UTF-8:GamBit Group\n` +
      (address ? `ADR;PREF=1;Address="${address}":;;${address}\n` : "") +
      `TEL;TYPE=Mobile:${profile.contactNo}\n` +
      `EMAIL;Email:${profile.email}\n` +
      `URL;TYPE=Profile:${window.location.href}\n` +
      (websiteLink ? `URL;TYPE=Work:${websiteLink}\n` : "") +
      base64Image +
      `END:VCARD`;

    const link = document.createElement("a");
    const file = new Blob([vcard], { type: "text/vcard" });
    link.href = URL.createObjectURL(file);
    link.download = profile.name.trim() + "'s Contact.vcf";
    link.click();
    URL.revokeObjectURL(link.href);
  };

  return (
    <Button
      size="lg"
      color="gray.100"
      bg="gray.700"
      _hover={{
        bg: "gray.500",
      }}
      textTransform="uppercase"
      letterSpacing={"0.25em"}
      fontSize="md"
      fontFamily="mono"
      rightIcon={<FaAddressBook />}
      aria-label="save contact"
      title="Save Contact"
      onClick={() => {
        saveCard(profile);
      }}
    >
      Save Contact
    </Button>
  );
}
