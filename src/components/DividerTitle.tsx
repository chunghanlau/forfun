import { Divider, Flex, Text } from "@chakra-ui/react";
import { ReactNode } from "react";

export default function DividerTitle({ children }: { children: ReactNode }) {
  return (
    <Flex pt={5} pb={10} alignItems="center">
      <Divider borderColor="black" />
      <Text textTransform="uppercase" letterSpacing={"0.25em"} fontSize="xl" fontFamily="mono" px={5}>
        {children}
      </Text>
      <Divider borderColor="black" />
    </Flex>
  );
}
