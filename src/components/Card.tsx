import {
  Box,
  Flex,
  Icon,
  IconButton,
  Image,
  Link,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  SimpleGrid,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Head from "next/head";
import icons from "../profiles/icons";
import { Profile } from "../profiles/types";
import DividerTitle from "./DividerTitle";
import IconItem from "./IconItem";
import MapBox from "./MapBox";
import dynamic from "next/dynamic";
const SaveVCardButton = dynamic(() => import("./SaveVCardButton"), { ssr: false });

export default function Card({ profile }: { profile: Profile }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Head>
        <title>{"Gambit - " + profile.name}</title>
        <meta name="description" content={profile.title} />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:image" content={`https://card.gambit.com.my${profile.pictureURI}`} />
        <meta property="og:title" content={profile.name} />
        <meta property="og:description" content={profile.title} />
        <meta property="og:image:width" content="192" />
        <meta property="og:image:height" content="192" />
      </Head>

      <Flex w="100vw" minH="100vh" flexDir="column" alignItems="center" pt={10} pb={20}>
        <Box
          border="2px solid"
          borderColor="gray.200"
          padding="0.5"
          rounded="full"
          onClick={onOpen}
          cursor="pointer"
          transition="all .2s ease-in-out"
          boxShadow="5px 0px 20px rgba(0,0,0,0.1)"
          _hover={{ transform: "scale(1.02)", boxShadow: "5px 5px 20px rgba(0,0,0,0.1)" }}
        >
          <Image
            src={profile.pictureURI}
            w={{ base: "150", md: "200" }}
            h={{ base: "150", md: "200" }}
            alt=""
            rounded="full"
          />
        </Box>
        <Box pt={2} pb={5} textAlign="center">
          <Text fontSize="2xl" fontWeight="semibold">
            {profile.name}
          </Text>
          <Text fontSize="xs" fontFamily="mono" letterSpacing={"0.1em"} textTransform="uppercase">
            {profile.title}
          </Text>
        </Box>

        <SaveVCardButton profile={profile} />
        <SimpleGrid py={5} gap={5} columns={3}>
          <IconButton
            as={Link}
            href={`tel:${profile.contactNo}`}
            target="_blank"
            icon={<Icon as={icons["phone"]} />}
            size={{ base: "lg", md: "lg" }}
            color="gray.100"
            bg="gray.700"
            _hover={{
              bg: "gray.500",
            }}
            aria-label="call"
            title="Call"
          />
          <IconButton
            as={Link}
            size={{ base: "lg", md: "lg" }}
            href={`mailto:${profile.email}`}
            target="_blank"
            icon={<Icon as={icons["email"]} />}
            color="gray.100"
            bg="gray.700"
            _hover={{
              bg: "gray.500",
            }}
            aria-label="email"
            title="Email"
          />
          <IconButton
            size={{ base: "lg", md: "lg" }}
            icon={<Icon as={icons["share"]} />}
            color="gray.100"
            bg="gray.700"
            _hover={{
              bg: "gray.500",
            }}
            aria-label="share"
            title="Share"
            onClick={() => {
              if (navigator.share) {
                navigator.share({
                  title: profile.name,
                  url: window.location.href,
                });
              }
            }}
          />
        </SimpleGrid>
        <Box w="full" maxW="lg" pt={5} px={{ base: 6, md: 14 }} bg="gray.50" rounded="lg" mt={5}>
          <Tabs size="sm" colorScheme="blackAlpha" align="center" variant="unstyled">
            <TabList>
              <Tab
                fontWeight="semibold"
                px={10}
                mx={1}
                color="gray.600"
                rounded="lg"
                _selected={{ color: "white", bg: "gray.700", _hover: { bg: "gray.700" } }}
                _hover={{
                  bg: "gray.200",
                }}
              >
                ABOUT
              </Tab>
              <Tab
                fontWeight="semibold"
                px={10}
                mx={1}
                color="gray.600"
                rounded="lg"
                _selected={{ color: "white", bg: "gray.700", _hover: { bg: "gray.700" } }}
                _hover={{
                  bg: "gray.200",
                }}
              >
                LINKS
              </Tab>
            </TabList>
            <TabPanels>
              <TabPanel px={0}>
                <DividerTitle>about</DividerTitle>
                <Stack gap={4}>
                  {profile.about.map((value, idx) => (
                    <IconItem key={idx} {...value} />
                  ))}
                  <MapBox mapLink={profile.mapLink} />
                </Stack>
              </TabPanel>
              <TabPanel px={0}>
                <DividerTitle>links</DividerTitle>
                <Stack gap={4}>
                  {profile.links.map((value, idx) => (
                    <IconItem key={idx} {...value} />
                  ))}
                </Stack>
              </TabPanel>
            </TabPanels>
          </Tabs>
          <DividerTitle>contact</DividerTitle>
          <Stack gap={4}>
            <IconItem icon="phone" label="contact no" description={profile.contactNo} />
            <IconItem icon="email" label="email" description={profile.email} />
          </Stack>
        </Box>
      </Flex>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalBody p={0}>
            <Image src={profile.pictureURI} alt="" w="full" maxW="600" />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
