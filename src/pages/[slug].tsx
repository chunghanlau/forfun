import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from "next";
import Card from "../components/Card";
import profiles from "../profiles";
import { Profile } from "../profiles/types";

export default function BusinessCard({ profile }: { profile: Profile }) {
  return <Card profile={profile} />;
}
export const getStaticPaths: GetStaticPaths = async () => {
  const paths = profiles.map((i) => ({ params: { slug: i.slug } }));
  return {
    paths,
    fallback: false,
  };
};
export const getStaticProps: GetStaticProps = async ({ params }: GetStaticPropsContext) => {
  const card = profiles.find((i) => i.slug === params?.slug || "");
  return {
    props: {
      profile: card?.profile,
    },
  };
};
