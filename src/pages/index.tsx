import {
  Box,
  Flex,
  IconButton,
  Image,
  Link,
  SimpleGrid,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
} from "@chakra-ui/react";
import Head from "next/head";
import {
  FaBriefcase,
  FaEnvelope,
  FaFacebookF,
  FaGlobeAsia,
  FaInstagram,
  FaLinkedinIn,
  FaList,
  FaMapMarkerAlt,
  FaPhone,
  FaShareAlt,
  FaTwitter,
} from "react-icons/fa";
import DividerTitle from "../components/DividerTitle";
import IconItem from "../components/IconItem";
import MapBox from "../components/MapBox";

const data = {
 
};

export default function Home() {
  return (
    <>
      <Head>
        <title>Gambit - Business Cards</title>
        <meta name="description" content="" />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:image" content={`https://card.gambit.com.my/assets/logo.webp`} />
        <meta property="og:title" content="Gambit - Business Cards" />
        <meta property="og:image:width" content="192" />
        <meta property="og:image:height" content="192" />
      </Head>

      <Flex w="100vw" minH="100vh" flexDir="column" alignItems="center" justifyContent="center" py={10}>
        <Image src="/assets/logo.webp" alt="logo" height={250} />
        <Text mt={5} textTransform="uppercase" fontFamily="mono" letterSpacing={"0.1em"} fontSize="lg">
          Business Cards
        </Text>
      </Flex>
    </>
  );
}
